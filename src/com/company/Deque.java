/**
 * Created by bogdan on 10/4/2015.
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A double-ended queue
 * @param <Item> template argument.
 */
public class Deque<Item> implements Iterable<Item> {
    /**
     * A double-ended queue.
     */
    private List<Item> deque;

    /**
     * Construct an empty deque.
     */
    public Deque() {

        this.deque = new ArrayList<>();
    }

    /**
     * Is the deque empty?
     * @return is the deque empty?
     */
    public boolean isEmpty() {

        return deque.size() == 0;
    }

    /**
     * Return the number of items on the deque.
     * @return the number of items on the deque.
     */
    public int size() {

        return deque.size();
    }

    /**
     * Add the item to the front.
     * @exception java.lang.NullPointerException if item is null.
     * @param item element.
     */
    public void addFirst(Item item) {
        if (item == null) {
            throw new java.lang.NullPointerException();
        }
        deque.add(0, item);
    }

    /**
     * Add the item to the end.
     * @exception java.lang.NullPointerException if item is null.
     * @param item element.
     */
    public void addLast(Item item) {
        if (item == null) {
            throw new java.lang.NullPointerException();
        }
        deque.add(item);
    }

    /**
     * Remove and return the item from the front.
     * @exception java.util.NoSuchElementException if deque is empty.
     * @return removed element.
     */
    public Item removeFirst() {
        if (deque.size() <= 0) {
            throw new java.util.NoSuchElementException();
        }
        return deque.remove(0);
    }

    /**
     * Remove and return the item from the end.
     * @exception java.util.NoSuchElementException if deque is empty.
     * @return removed element.
     */
    public Item removeLast() {
        if (deque.size() <= 0) {
            throw new java.util.NoSuchElementException();
        }
        return deque.remove(deque.size() - 1);
    }

    /**
     * Return an iterator.
     */
    @Override
    public Iterator<Item> iterator() {

        return new DequeIterator();
    }

    /**
     * Deque Iterator.
     */
    private class DequeIterator implements Iterator<Item> {
        /**
         * Elements in current double-ended queue.
         */
        private List<Item> cursor;
        /**
         * Cursor position.
         */
        private int current;

        /**
         * Construct an deque iterator.
         */
        public DequeIterator() {
            cursor = deque;
            current = 0;
        }

        /**
         * Remove from the iterator, the method does not supported.
         * @exception java.lang.UnsupportedOperationException
         */
        @Override
        public void remove() {

            throw new java.lang.UnsupportedOperationException();
        }

        /**
         * Check: exists any element or not in the cursor.
         * @return exists any element or not in the cursor.
         */
        @Override
        public boolean hasNext() {

            return current < cursor.size();
        }

        /**
         * Get the next element.
         * @return the next
         */
        @Override
        public Item next() {
            if (!this.hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            Item element = cursor.get(current);
            current++;
            return element;
        }
    }

    /**
     * Method for Unit testing.
     * @param args input arguments
     */
    public static void main(String[] args) {
        Deque<Integer> deck = new Deque<>();
        System.out.println(deck.isEmpty());
        System.out.println(deck.size());
        try {
            deck.addFirst(null);
        } catch (NullPointerException ignore) {
            System.out.println("Try to add to the front NULL");
        }
        try {
            deck.addLast(null);
        } catch (NullPointerException ignore) {
            System.out.println("Try to add to the end NULL");
        }
        deck.addFirst(2);
        deck.addLast(3);
        deck.addFirst(4);
        deck.addFirst(2);
        deck.addLast(3);
        deck.addFirst(2);
        deck.removeFirst();
        deck.removeLast();
        System.out.println(deck.isEmpty());
        System.out.println(deck.size());
        for (Object aDeck : deck) {
            System.out.println(aDeck);
        }
        Iterator it = deck.iterator();
        try {
            it.remove();
        } catch (java.lang.UnsupportedOperationException ignore) {
            System.out.println("Method was not implemented");
        }
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        try {
            System.out.println(it.next());
        } catch (java.util.NoSuchElementException ignore) {
            System.out.println("NO ELEMENTS");
        }
    }
}
