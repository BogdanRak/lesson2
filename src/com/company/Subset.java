/**
 * Created by bogdan on 10/6/2015.
 */

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

/**
 * Subset class.
 */
public class Subset {
    /**
     * Command line client.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        int subsetSize = Integer.parseInt(args[0]);
        String[] inputStrings = StdIn.readStrings();
        RandomizedQueue<String> queue = new RandomizedQueue<String>();
        // Input
        for (String str: inputStrings) {
            queue.enqueue(str);
        }
        // Output
        for (int j = 0; j < subsetSize; ++j) {
            StdOut.println(queue.dequeue());
        }
    }
}
