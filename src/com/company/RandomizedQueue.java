/**
 * Created by bogdan on 10/4/2015.
 */

import edu.princeton.cs.algs4.StdRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A randomized queue is similar to a stack or queue.
 * @param <Item> template argument.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    /**
     * Queue.
     */
    private List<Item> queue;

    /**
     * Construct an empty randomized queue.
     */
    public RandomizedQueue() {

        queue = new ArrayList<Item>();
    }

    /**
     * Is the queue empty?
     * @return is the queue empty?
     */
    public boolean isEmpty() {

        return queue.size() == 0;
    }

    /**
     * Return the number of items on the queue.
     * @return the number of items on the queue.
     */
    public int size() {

        return queue.size();
    }

    /**
     * Add the item.
     * @exception java.lang.NullPointerException if item is null.
     * @param item element.
     */
    public void enqueue(Item item) {
        if (item == null) {
            throw new java.lang.NullPointerException();
        }
        queue.add(item);
    }

    /**
     * Remove and return a random item.
     * @exception java.util.NoSuchElementException if queue is empty.
     * @return removed item.
     */
    public Item dequeue() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        int randIdx = StdRandom.uniform(size());
        return queue.remove(randIdx);
    }

    /**
     * Return (but do not remove) a random item.
     * @exception java.util.NoSuchElementException if queue is empty.
     * @return random item.
     */
    public Item sample() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException();
        }
        int randIdx = StdRandom.uniform(size());
        return queue.get(randIdx);
    }

    /**
     * Return an independent iterator over items in random order.
     */
    @Override
    public Iterator<Item> iterator() {

        return new QueueIterator();
    }

    /**
     * Queue Iterator.
     */
    private class QueueIterator implements Iterator<Item> {
        /**
         * Cursor of the queue.
         */
        private Object[] cursor;

        /**
         * Position.
         */
        private int current;

        /**
         * Construct a queue iterator.
         */
        public QueueIterator() {
            cursor = new Object[queue.size()];
            queue.toArray(cursor);
            StdRandom.shuffle(cursor);
            current = 0;
        }

        /**
         * Has a next item ?
         * @return has a next item ?
         */
        @Override
        public boolean hasNext() {

            return current < cursor.length;
        }

        /**
         * Getting the next item in the iterator.
         * @exception java.util.NoSuchElementException has no any elements.
         * @return a next item.
         */
        @Override
        public Item next() {
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }
            Item nextItem = (Item) cursor[current];
            current++;
            return nextItem;
        }

        /**
         * Remove element from the iterator.
         * @exception java.lang.UnsupportedOperationException
         */
        @Override
        public void remove() {

            throw new java.lang.UnsupportedOperationException();
        }
    }

    /**
     * Unit testing.
     * @param args command line arguments.
     */
    public static void main(String[] args) {
        RandomizedQueue<Integer> queue = new RandomizedQueue();
        queue.enqueue(1);
        System.out.println(queue.sample());
        System.out.println(queue.dequeue());
    }
}
